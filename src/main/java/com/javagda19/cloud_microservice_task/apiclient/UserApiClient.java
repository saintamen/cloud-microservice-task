package com.javagda19.cloud_microservice_task.apiclient;

import com.javagda19.cloud_microservice_task.model.AppUserDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseStatus;

@FeignClient("cloud-microservice-user")
public interface UserApiClient {
    @GetMapping("/user/{id}")
    public AppUserDto get(@PathVariable(name = "id") Long id);
}
